FROM openjdk:12
ADD target/docker-spring-boot-andre_follador.jar docker-spring-boot-andre_follador.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-andre_follador.jar"]

