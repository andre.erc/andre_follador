package dockerspringbootandre_follador.dockerspringbootandre_follador;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DockerSpringBoot {

	public static void main(String[] args) {
		SpringApplication.run(DockerSpringBoot.class, args);
	}

}
